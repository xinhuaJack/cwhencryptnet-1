<?php

namespace CWHEncryptNet;

/**
 * Class CWHEncryptNetLogic
 * 來源： https://www.h3399.cn/201710/160445.html
 *
 * @package common\logics
 */
class CWHEncryptNetLogic
{
    CONST ENCRYPT_KEY_LEN = 8;

    /**
     * 数据解密
     * @param encrypData
     * @return
     */
    public static function XorCrevasse($encrypData)
    {
        $builder = "";
        $length = strlen($encrypData);
        if ($length < self::ENCRYPT_KEY_LEN * 8) return "aaa";

        $num2 = intval(substr($encrypData,0, 4), 0x10);

        if ($length != ((((int)((($num2 + self::ENCRYPT_KEY_LEN) - 1) / self::ENCRYPT_KEY_LEN)) * self::ENCRYPT_KEY_LEN) * 8)) return "bbb" . $length;

        for ($i = 0; $i < $num2; $i++) {
            $str2 = substr($encrypData, $i*8, 4);
            $str = substr($encrypData, $i*8+4, 4);
            $num4 = intval($str2,0x10);
            $num5 = intval($str,0x10);
            $builder .= chr($num4 ^ $num5);
        }
        return $builder;
    }

    /**
     * 数据加密
     * @param sourceData
     * @return
     */
    public static function XorEncrypt($sourceData) {
        $builder = "";
        $numArray = [];
        $numArray[0] = strlen($sourceData);

        for ($i = 1; $i < self::ENCRYPT_KEY_LEN; $i++) {
            $numArray[$i] = (int) (((rand(1,9999)/10000)*0xffff) % 0xffff);
        }
        $num2 = 0;
        $num3 = (int) (((int)((($numArray[0] + self::ENCRYPT_KEY_LEN) - 1) / self::ENCRYPT_KEY_LEN)) * self::ENCRYPT_KEY_LEN);
        $sourceArr = $sourceData;
        for ($j = 0; $j < $num3; $j++) {
            if ($j < $numArray[0]) {
                $num2 = (self::hashCode($sourceArr[$j]) ^ $numArray[$j % self::ENCRYPT_KEY_LEN]);
            } else {
                $num2 = (int) ($numArray[$j % self::ENCRYPT_KEY_LEN] ^ ((int) (((rand(1,9999)/10000)*0xffff) % 0xffff)));
            }
            $builder .= str_pad(dechex($numArray[$j % self::ENCRYPT_KEY_LEN]),4,"0", STR_PAD_LEFT) . str_pad(dechex($num2),4,"0", STR_PAD_LEFT);
        }

        return strtoupper($builder);
    }

    private static function overflow32($v)
    {
        $v = $v % 4294967296;
        if ($v > 2147483647) return $v - 4294967296;
        elseif ($v < -2147483648) return $v + 4294967296;
        else return $v;
    }

    private static function hashCode( $s )
    {
        $h = 0;
        $len = strlen($s);
        for($i = 0; $i < $len; $i++)
        {
            $h = self::overflow32(31 * $h + ord($s[$i]));
        }

        return $h;
    }
}